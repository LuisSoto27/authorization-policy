<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class AuthorizationController extends Controller
{
    public function index()
    {
        Gate::allows('Admin') ? Response::allow() : abort(403);
        return "Autorizado";

        /* if(Gate::allows('Admin')){
            return "Autorizado";
        }
        else{
            abort(403);
        }; */
    }
}


