<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(User $user)
    {
        $this->authorize('viewAny', $user);

        $users = User::orderBy('id', 'asc')->paginate(10);

        return view('users.index', compact('users'));
    }

    public function create(User $user)
    {
        $this->authorize('create', $user);

        $user = new User(); 
        
        return view ('users.create', compact('user'));
    }

    public function store(Request $request, User $user)
    {

        $this->authorize('create', $user);

        $user = new User();

        $user->name = $request->name;
        $user->email = $request->email; 
        $user->password = $request->password;
        $user->role = $request->role;

        $user->save();
        
        return back()->with('status', 'El usuario fue creado exitosamente!');
    }

    public function show($id)
    {
        //
    }

    public function edit(User $user)
    {
        $this->authorize('update', $user);

        return view('users.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $this->authorize('update', $user);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->save();

        return back()->with('status', 'El usuario fue editado exitosamente!');
    }

    public function destroy($id)
    {
        //
    }
}
