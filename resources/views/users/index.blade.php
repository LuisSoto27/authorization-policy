@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-person-lines-fill"></i> Lista de Usuarios</h1>
                    <div class="mb-4 mt-4">
                        <div class="bg-white border shadow-sm p-3 mt-4">
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Rol</th>
                                        <th scope="col">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                    <tr>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{ucfirst($user->role)}}</td>
                                        <td>
                                            <div class="btn-group" role="group">
                                                <a href="{{ route('user.edit', $user->id)}}" role="button" class="btn btn-sm btn-outline-dark"><i class="bi bi-pen"></i> Editar</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <a href="{{ route('user.create') }}" class="btn btn-success">Crear</a>
                            <a href="{{ route('home') }}" class="btn btn-primary">Regresar</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--@i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection
