@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-plus"></i> Añadir Usuario</h1>
                    @include('messages')
                    <div class="row">
                        <div class="col-md-5 mb-4">
                            <div class="p-3 border bg-light shadow">
                                <form action="{{route('user.store')}}" method="POST">
                                @csrf
                                    <div class="col-md-9">
                                        <label for="name" class="form-label">Nombre<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Nombre" required value="{{old('name')}}">
                                    </div>
                                    <br>
                                    <div class="col-md-9">
                                        <label for="email" class="form-label">Correo<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="usuario@ejemplo.com" required value="{{old('email')}}">
                                    </div>
                                    <br>
                                    <div class="col-md-9">
                                        <label for="password" class="form-label">Contraseña<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                        <input type="password" class="form-control" id="password" name="password" required>
                                    </div>
                                    <br>
                                    <div class="col-md-9">
                                        <label for="role" class="form-label">Rol<sup><i class="bi bi-asterisk text-primary"></i></sup></label>
                                        <select id="role" class="form-select" name="role" required>
                                            <option value="admin" {{old('role') == 'admin' ? 'selected' : ''}}>Administrador</option>
                                            <option value="user" {{old('role') == 'user' ? 'selected' : ''}}>Usuario</option>
                                            <option value="editor" {{old('role') == 'editor' ? 'selected' : ''}}>Editor</option>
                                        </select>
                                    </div>
                                    <br>
                                    <button class="btn btn-sm btn-outline-success" type="submit"><i class="bi bi-check2"></i> Crear</button>
                                    <a class="btn btn-sm btn-outline-secondary" href="{{ route ('user.index') }}"><i class="bi bi-arrow-counterclockwise"></i> Regresar</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
