@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Bienvenido, por favor, selecciona una opción.</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        @can('Admin')
                            <h5>Hola, has ingresado al sistema como Administrador.</h5>
                            <p>Utiliza el Gate solo si eres Administrador.</p>
                        @endcan

                        @can('User')
                            <h5>Hola, has ingresado al sistema como Usuario.</h5>
                        @endcan

                        @can('Editor')
                            <h5>Hola, has ingresado al sistema como Editor.</h5>
                        @endcan

                    <a href="{{route ('gate.index')}}" class="btn btn-primary">Gate</a>
                    <hr>
                    <a href="{{route ('user.index')}}" class="btn btn-info">Usuarios</a>
                    <hr>
                    <a href="{{route ('post.index')}}" class="btn btn-success">Posts</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
