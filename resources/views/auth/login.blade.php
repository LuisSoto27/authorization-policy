@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <br><br><br><br>
            <div class="card">

                <div class="card-body shadow">

                    <div class="text-center">
                        <img src="{{ URL::asset('/image/logo.png') }}" class="rounded" alt="Logo de Prueba" height="100" width="100">
                    </div>
                    <br>
                    <div class="text-center mb-3">
                        <h5>Iniciar Sesión</h5>
                        <span class="d-block text-muted">Ingresa tus datos</span>
                    </div>

                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="row mb-3 justify-content-center">
                            <div class="col-lg-8 col-lg-offset-8">
                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" placeholder="Correo Electrónico" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3 justify-content-center">
                            <div class="col-lg-8 col-lg-offset-8">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Contraseña" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group d-flex mb-3">
                            <div class="p-2">
                                <label class="form-check-label"> 
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    Recuérdame
                                </label>
                            </div>
                            <div class="ms-auto p-2">
                                @if (Route::has('password.request'))
                                    <a href="{{ route('password.request') }}">¿Olvidaste tu Contraseña?</a>
                                @endif
                            </div>                           
                        </div>
                    
                        <div class="d-grid gap-2 col-8 mx-auto">
                            <button type="submit" class="btn btn-success">Ingresar</button>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
