@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <div class="row">
                    <h3>Editar Post: {{$post->title}}</h3>
                    @include('messages')
                        <div class="col-md-5 mb-4">
                            <div class="p-3 border bg-light shadow">
                                <form action="{{ route ('post.update', $post->id) }} " method="POST">
                                @method ('PUT')
                                @csrf
                                    <div class="mb-4">
                                        <label for="title" class="form-label">Título: </label>
                                        <input type="text" class="form-control" id="title" name="title" value="{{ $post->title }}" required>
                                    </div>
                                    <div class="mb-4">
                                        <label for="description" class="form-label">Descripción: </label>
                                        <input type="text" class="form-control" id="description" name="description" value="{{ $post->description }}" required>
                                    </div>
                                    <button class="btn btn-sm btn-outline-success" type="submit"><i class="bi bi-check2"></i> Actualizar</button>
                                    <a class="btn btn-sm btn-outline-secondary" href="{{ route ('post.index') }}"><i class="bi bi-arrow-counterclockwise"></i> Regresar</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- @i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection
