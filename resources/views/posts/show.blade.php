@extends('layouts.app')

@section('content')
<div class="container">
    <h3>{{ $post->title }}</h3>
    <hr>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="width: 30rem;">
                <div class="card-body">
                  <h5 class="card-title">{{ $post->title }}</h5>
                  <p class="card-text">{{ $post->description }}</p>
                  <p class="card-text">Creado por: {{ $post->user->name }}</p>
                  <a href="{{ route('post.index') }}" class="btn btn-primary">Regresar</a>
                </div>
              </div>
        </div>
    </div>
</div>
@endsection