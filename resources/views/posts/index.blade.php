@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-start">
        <div class="col-xs-11 col-sm-11 col-md-11 col-lg-10 col-xl-10 col-xxl-10">
            <div class="row pt-2">
                <div class="col ps-4">
                    <h1 class="display-6 mb-3"><i class="bi bi-plus"></i> Lista de Posts</h1>
                    @include('messages')
                    <div class="row">
                        <div class="col-md-9 mb-4">
                            <div class="p-3 border bg-light shadow-sm">
                                <table class="table table-striped table-hover">
                                    <thead class="thead">
                                        <tr>
                                        <th>Titulo</th>
                                        <th>Descripción</th>
                                        <th>Autor</th>
                                        
                                        <th>Opción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($posts as $post)
                                            <tr>
                                                
                                                <td>{{ $post->title }}</td>
                                                <td>{{ $post->description }}</td>
                                                <td>{{ $post->user->name}}</td>

                                                <td style="width:120px">
                                                  <a href="{{ route ('post.show', $post) }}" class="btn btn-info btn-sm" title="Ver Post"><i class="bi bi-reply-all-fill"></i></a>

                                                  @can ('update', $post)  
                                                    <a href="{{ route ('post.edit', $post) }}" class="btn btn-warning btn-sm" title="Editar Post"><i class="bi bi-pencil-square"></i></a>
                                                  @endcan

                                                  @can ('delete', $post)  
                                                    <a href="{{ route ('post.delete', $post) }}" class="btn btn-danger btn-sm" title="Eliminar Post"><i class="bi bi-bag-x"></i></a>
                                                  @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <a href="{{ route('home') }}" class="btn btn-primary">Regresar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- @i('layouts.footer') -->
        </div>
    </div>
</div>
@endsection
