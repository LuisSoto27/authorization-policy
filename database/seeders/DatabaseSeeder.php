<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Post;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        User::create([
            'name' => 'Luis',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456789'),
            'role' => 'admin'
        ]);
    
        User::create([
            'name' => 'Steve',
            'email' => 'user@gmail.com',
            'password' => bcrypt('123456789'),
            'role' => 'user'
        ]);
    
        User::create([
            'name' => 'Leo',
            'email' => 'editor@gmail.com',
            'password' => bcrypt('123456789'),
            'role' => 'editor'
        ]);
    
        Post::factory(8)->create();
    }
}
